# postest-group (Zanuarestu Ramadhani & Trisna Tera Soedjita)

## Backend

Use https://github.com/trisnatera/posttest-individu

## Frontend Build Setup

change backendURL in file ``config.js``

``` bash
# build for production and launch server
$ yarn install
$ yarn build
$ yarn start
```

OR you can use NPM 
``` bash
# build for production and launch server
$ npm install
$ npm run build
$ npm run start
```